package account

import (
	"github.com/labstack/echo"
)

func SetRouters(g *echo.Group) {
	var handler = AccountHandler{}

	g.GET("/account-details", handler.GetDetail)
	g.GET("/deposits", handler.GetDeposits)
}
