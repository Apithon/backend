package account

// "time"

type ResponseAccountDetails struct {
	AccountDetails []AccountDetails `json:"data,omitempty"`
}

type AccountDetails struct {
	SavingsAccount []SavingsAccount `json:"savingsAccount,omitempty"`
	TimeDeposit    []TimeDeposit    `json:"timeDeposit,omitempty"`
	LoanAccount    []LoanAccount    `json:"loanAccount,omitempty"`
}

type SavingsAccount struct {
	AccountNumber                  string `json:"account_number,omitempty"`
	SavingsAccountAvailableBalance string `json:"savingsAccount_availableBalance,omitempty"`
}

type TimeDeposit struct {
	FinancialProductCode                    string `json:"financialProduct_code,omitempty"`
	FinancialProductOriginalPrincipalAmount string `json:"financialProduct_originalPrincipalAmount,omitempty"`
	TimeDepositInterestRate                 string `json:"timeDeposit_interestRate,omitempty"`
	TimeDepositOpeningDate                  string `json:"timeDeposit_openingDate,omitempty"`
	TimeDepositMaturityDate                 string `json:"timeDeposit_maturityDate,omitempty"`
}

type LoanAccount struct {
	FinancialProductCode              string `json:"financialProduct_code,omitempty"`
	LoanAccountCurrentPrincipalAmount string `json:"loanAccount_currentPrincipalAmount,omitempty"`
	LoanAccountCurrencyCode           string `json:"loanAccount_currencyCode,omitempty"`
	LoanAccountMaturityDate           string `json:"loanAccount_maturityDate,omitempty"`
	LoanAccountOpeningDate            string `json:"loanAccount_openingDate,omitempty"`
	LoanAccountInterestAmount         string `json:"loanAccount_interestAmount,omitempty"`
	LoanAccountNextPaymentAmount      string `json:"loanAccount_nextPaymentAmount,omitempty"`
	LoanAccountNextPaymentDate        string `json:"loanAccount_nextPaymentDate,omitempty"`
	LoanAccountInterestType           string `json:"loanAccount_interestType,omitempty"`
}
