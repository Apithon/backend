package account

// "time"

type ResponseAccountDeposit struct {
	AccountDeposits []AccountDeposits `json:"data,omitempty"`
}

type AccountDeposits struct {
	TypeAccount             string `json:"typeAccount,omitempty"`
	NumberAccount           string `json:"numberAccount,omitempty"`
	OverdraftQuota          int    `json:"overdraftQuota,omitempty"`
	CashBalance             int    `json:"cashBalance,omitempty"`
	ExchangeBalance         int    `json:"exchangeBalance,omitempty"`
	ReceivableBalance       int    `json:"receivableBalance,omitempty"`
	ExchangeBalanceStartDay int    `json:"exchangeBalanceStartDay,omitempty"`
	CashBalanceStartDay     int    `json:"cashBalanceStartDay,omitempty"`
}
