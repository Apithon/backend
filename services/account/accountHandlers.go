package account

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/labstack/echo"

	config "backend/config"
	util "backend/util"
)

type AccountHandler struct{}

func (AccountHandler) GetDetail(c echo.Context) error {

	url := "https://sbapi.bancolombia.com/v1/operations/product-specific/accounts/account-details?page_size=10&page_key=1"
	req, _ := http.NewRequest("GET", url, nil)

	// req.Header.Add("authorization", "Bearer AAIkZWYwNTdmNWUtY2NiNy00MTdiLTg4MDMtNzY1YThmY2NhODk0rLkN9xQ6F2GUgO33m5Dul5FNfmShIc65Nsy5pDaXkgOvCk74bHapKg62_oBDHXuaSdLCCB4CwjP6jhNueaYNhtYE7k8h1owvWMLPlXLX9BSyJT8Dyc3MVk4H2YQ2j5mJNYHG9mpoU7g0Zel0yrgAhw")
	// fmt.Println(config.TokenType + " " + config.Token)
	req.Header.Add("authorization", config.TokenType+" "+config.Token)
	req.Header.Add("accept", "application/vnd.bancolombia.v1+json")

	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	// body, _ := ioutil.ReadAll(res.Body)

	// fmt.Println(res)
	// fmt.Println(string(body))

	// Validamos el statusCode es diferente de SUCCESS
	if !util.IsSuccessStatusCode(res.StatusCode) {
		body, _ := ioutil.ReadAll(res.Body)
		fmt.Println(string(body))
		return c.JSON(res.StatusCode, res.Status)
	}

	var response ResponseAccountDetails
	err := util.ConvertBodyToEntity(res.Body, &response)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusCreated, &response.AccountDetails)
}

func (AccountHandler) GetDeposits(c echo.Context) error {

	url := "https://sbapi.bancolombia.com/v2/operations/cross-product/account-management/position-keeping/deposits?_pageNumber=10&_pageSize=1"
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("authorization", config.TokenType+" "+config.Token)
	req.Header.Add("accept", "application/vnd.bancolombia.v1+json")

	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	// body, _ := ioutil.ReadAll(res.Body)

	// fmt.Println(res)
	// fmt.Println(string(body))

	// Validamos el statusCode es diferente de SUCCESS
	if !util.IsSuccessStatusCode(res.StatusCode) {
		body, _ := ioutil.ReadAll(res.Body)
		fmt.Println(string(body))
		return c.JSON(res.StatusCode, res.Status)
	}

	var response ResponseAccountDeposit
	err := util.ConvertBodyToEntity(res.Body, &response)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusCreated, &response.AccountDeposits)
}
