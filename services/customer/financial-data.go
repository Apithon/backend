package customer

// "time"

type ResponseFinancialData struct {
	ResponseData []ResponseData `json:"data,omitempty"`
}

type ResponseData struct {
	FinancialData FinancialData `json:"financialData,omitempty"`
}

type FinancialData struct {
	TaxDeclarant     string `json:"tax_declarant,omitempty"`
	IncomeMonth      string `json:"income_month,omitempty"`
	IncomeMonthOther string `json:"income_monthOther,omitempty"`
	IncomeMonthTotal string `json:"income_monthTotal,omitempty"`
	EgressMonthTotal string `json:"egress_monthTotal,omitempty"`
	LiabilitiesTotal string `json:"liabilities_total,omitempty"`
	Patrimony        string `json:"patrimony,omitempty"`
	RevenueTotalDate string `json:"revenue_totalDate,omitempty"`
	RevenueReal      string `json:"revenue_Real,omitempty"`
	Retainer         string `json:"retainer,omitempty"`
	SelfRetainer     string `json:"self-retainer,omitempty"`
	TaxpayerClass    string `json:"taxpayer_class,omitempty"`
	TaxpayerType     string `json:"taxpayer_type,omitempty"`
	CurrencyCode     string `json:"currency_code,omitempty"`
	IvaRegime        string `json:"iva_regime,omitempty"`
	CostTotal        string `json:"cost_total,omitempty"`
}
