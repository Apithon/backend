package customer

import (
	"github.com/labstack/echo"
)

func SetRouters(g *echo.Group) {
	var handler = CustomerHandler{}

	g.GET("/financial-data", handler.GetFinancialData)
}
