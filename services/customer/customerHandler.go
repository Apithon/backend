package customer

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/labstack/echo"

	config "backend/config"
	util "backend/util"
)

type CustomerHandler struct{}

func (CustomerHandler) GetFinancialData(c echo.Context) error {

	url := "https://sbapi.bancolombia.com/v1/sales-services/customer-management/customer-precedents/customers/financial-data"
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("authorization", config.TokenType+" "+config.Token)
	req.Header.Add("accept", "application/vnd.bancolombia.v1+json")

	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	// body, _ := ioutil.ReadAll(res.Body)

	// fmt.Println(res)
	// fmt.Println(string(body))

	// Validamos el statusCode es diferente de SUCCESS
	if !util.IsSuccessStatusCode(res.StatusCode) {
		body, _ := ioutil.ReadAll(res.Body)
		fmt.Println(string(body))
		return c.JSON(res.StatusCode, res.Status)
	}

	var response ResponseFinancialData
	err := util.ConvertBodyToEntity(res.Body, &response)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusCreated, &response.ResponseData)
}
