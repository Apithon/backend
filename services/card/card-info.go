package account

// "time"

type ResponseCardInfo struct {
	ResponseData []ResponseCardInfoData `json:"data,omitempty"`
}

type ResponseCardInfoData struct {
	CardNumber                 string  `json:"card_number,omitempty"`
	CardAlias                  string  `json:"card_alias,omitempty"`
	CardAvalaibleBalance       int     `json:"card_avalaibleBalance,omitempty"`
	CardState                  string  `json:"card_state,omitempty"`
	CardPayDate                string  `json:"card_payDate,omitempty"`
	CardPayMin                 float32 `json:"card_payMin,omitempty"`
	CardPayTotal               float32 `json:"card_payTotal,omitempty"`
	CardPayDollarMin           float32 `json:"card_payDollarMin,omitempty"`
	CardPayDollarTotal         float32 `json:"card_payDollarTotal,omitempty"`
	CardAvalaibleAvance        float32 `json:"card_avalaibleAvance,omitempty"`
	CardCurrentPrincipalAmount float32 `json:"card_currentPrincipalAmount,omitempty"`
}
