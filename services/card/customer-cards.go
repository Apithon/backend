package account

// "time"

type ResponseCustomerCards struct {
	ResponseData []ResponseCustomerCardsData `json:"data,omitempty"`
}

type ResponseCustomerCardsData struct {
	CustomerCards CustomerCards `json:"cards,omitempty"`
	Status        Status        `json:"status,omitempty"`
}

type CustomerCards struct {
	CardNumber           string `json:"card_number,omitempty"`
	CardType             string `json:"card_type,omitempty"`
	CardFranchise        string `json:"card_franchise,omitempty"`
	CardAvailableBalance int    `json:"card_availableBalance,omitempty"`
}

type Status struct {
	Description string `json:"description,omitempty"`
}
