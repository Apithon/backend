package account

import (
	"github.com/labstack/echo"
)

func SetRouters(g *echo.Group) {
	var handler = CardHandler{}

	g.GET("", handler.GetCards)
	g.GET("/:id/detail", handler.GetDetail)
}
