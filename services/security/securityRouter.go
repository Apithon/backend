package security

import (
	"github.com/labstack/echo"
)

func SetRouters(g *echo.Group) {
	var handler = SecurityHandler{}

	g.POST("/:code", handler.GetToken)
}
