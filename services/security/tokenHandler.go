package security

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/labstack/echo"

	config "backend/config"
	util "backend/util"
)

type SecurityHandler struct{}

func (SecurityHandler) GetToken(c echo.Context) error {

	url := "https://sbapi.bancolombia.com/security/oauth-otp/oauth2/token"

	grant_type := "authorization_code"
	// code := "AAKTdaIxr6IzS4mFiwJgqLst9afiMA3H2D-r-G9dDgMgheKR_UnYADrjoxuV4ID0QKHVPUU8ZqW3FpsyChVoSPEBEvGmzDxPw5cWQGdjA70V4oJ2xPSYiSevF3xy7Jsy27PH1YvQMJe8l0SDXXeaAIl-"
	code := c.Param("code")

	// payload := strings.NewReader("grant_type=authorization_code&code=fefdoww&redirect_uri=aroona&username=Maria&password=kufinocu&scope=beuk&refresh_token=af81ad073c23dc3d41bade6db424ae481364eb68eaf7d30722ff133ced4ece20")
	payload := strings.NewReader("grant_type=" + grant_type + "&code=" + code + "&redirect_uri=" + config.RedirectUri + "&client_id=" + config.ClientID + "&client_secret=" + config.ClientSecret)

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	req.Header.Add("accept", "application/json")

	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	// body, _ := ioutil.ReadAll(res.Body)

	// fmt.Println(res)
	// fmt.Println(string(body))

	// Validamos el statusCode es diferente de SUCCESS
	if !util.IsSuccessStatusCode(res.StatusCode) {
		body, _ := ioutil.ReadAll(res.Body)
		fmt.Println(string(body))
		return c.JSON(res.StatusCode, res.Status)
	}

	var token Token
	err := util.ConvertBodyToEntity(res.Body, &token)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	// config.TokenType = token.TokenType
	config.Token = token.AccessToken
	config.RefreshToken = token.RefreshToken
	return c.JSON(http.StatusCreated, &token)
}

func RefreshToken() error {

	if len(config.Token) == 0 {
		return nil
	}

	url := "https://sbapi.bancolombia.com/security/oauth-otp/oauth2/token"

	grant_type := "refresh_token"

	fmt.Println("*** ANTES ***")
	fmt.Println("Token ->", config.Token)
	fmt.Println("RefreshToken ->", config.RefreshToken)

	payload := strings.NewReader("grant_type=" + grant_type + "&client_id=" + config.ClientID + "&client_secret=" + config.ClientSecret + "&refresh_token=" + config.RefreshToken)
	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	req.Header.Add("accept", "application/json")

	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()

	// Validamos el statusCode es diferente de SUCCESS
	if !util.IsSuccessStatusCode(res.StatusCode) {
		return errors.New(res.Status)
	}

	var token Token
	err := util.ConvertBodyToEntity(res.Body, &token)

	if err != nil {
		return err
	}

	// config.TokenType = token.TokenType
	config.Token = token.AccessToken
	config.RefreshToken = token.RefreshToken

	fmt.Println("*** DESPUES ***")
	fmt.Println("Token ->", config.Token)
	fmt.Println("RefreshToken ->", config.RefreshToken)
	// fmt.Println("***************")
	fmt.Println("")

	return nil
}
