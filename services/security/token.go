package security

type Token struct {
	TokenType             string `json:"token_type,omitempty"`
	AccessToken           string `json:"access_token,omitempty"`
	ExpiresIn             int    `json:"expires_in,omitempty"`
	ConsentedOn           int64  `json:"consented_on,omitempty"`
	Scope                 string `json:"scope,omitempty"`
	RefreshToken          string `json:"refresh_token,omitempty"`
	RefreshTokenExpiresIn int64  `json:"refresh_token_expires_in,omitempty"`
}
