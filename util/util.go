package util

import (
	// "errors"
	// "strings"
	"encoding/json"
	// "net/http"
	"fmt"
	"io"
	"io/ioutil"
	// config "backend/config"
	// securityService "backend/services/security"
)

// Transformamos un body en la entidad
func ConvertBodyToEntity(body io.Reader, entity interface{}) error {

	// Obtenemos el body en bytes
	bodyArrayBytes, err := ioutil.ReadAll(body)
	if err != nil {
		fmt.Println("Failed reading the request body ->", err)
		return err
	}
	// fmt.Println("bodyArrayBytes -> ", string(bodyArrayBytes))

	// Mapeamos el body a la entidad
	err = json.Unmarshal(bodyArrayBytes, entity)
	return err
}

// IsSuccessStatusCode valida si el statusCode es success
func IsSuccessStatusCode(statusCode int) bool {
	return (statusCode >= 200) && (statusCode <= 299)
}

// func RefreshToken() error {

// 	url := "https://sbapi.bancolombia.com/security/oauth-otp/oauth2/token"

// 	grant_type := "refresh_token"

// 	fmt.Println("*** ANTES ***")
// 	fmt.Println("Token ->", config.Token)
// 	fmt.Println("RefreshToken ->", config.RefreshToken)

// 	payload := strings.NewReader("grant_type=" + grant_type + "&client_id=" + config.ClientID + "&client_secret=" + config.ClientSecret + "&refresh_token=" + config.RefreshToken)
// 	req, _ := http.NewRequest("POST", url, payload)

// 	req.Header.Add("content-type", "application/x-www-form-urlencoded")
// 	req.Header.Add("accept", "application/json")

// 	res, _ := http.DefaultClient.Do(req)
// 	defer res.Body.Close()

// 	// Validamos el statusCode es diferente de SUCCESS
// 	if !IsSuccessStatusCode(res.StatusCode) {
// 		return errors.New(res.Status)
// 	}

// 	var token securityService.Token
// 	err := ConvertBodyToEntity(res.Body, &token)

// 	if err != nil {
// 		return err
// 	}

// 	config.TokenType = token.TokenType
// 	config.Token = token.AccessToken
// 	config.RefreshToken = token.RefreshToken

// 	return nil
// }
