package main

import (
	"net/http"

	"backend/routers"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	securityService "backend/services/security"
)

func RefreshTokenMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	// return a HandlerFunc
	return func(c echo.Context) error {

		err := securityService.RefreshToken()

		// Validamos el username
		if err != nil {
			// return &echo.HTTPError{
			// 	Code:    http.StatusUnauthorized,
			// 	Message: authUtil.ErrorInvalidToken,
			// }
		}

		return next(c)
	}
}

func main() {
	e := echo.New()
	e.Use(RefreshTokenMiddleware)
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	routers.InitRoutes(e)

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, Api - apithon!")
	})
	e.Logger.Fatal(e.Start(":2000"))
}
