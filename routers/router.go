package routers

import (

	// "errors"
	"net/http"

	// "strconv"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	accountService "backend/services/account"
	cardService "backend/services/card"
	customerService "backend/services/customer"
	securityService "backend/services/security"
	// authUtil "woh/util-auth/util"
)

const PATH = "/apithon"

func InitRoutes(e *echo.Echo) {

	// Middlewares
	e.Use(middleware.Recover())

	e.GET(PATH+"/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, Api - Profesionales! 2")
	})

	securityGroup := e.Group(PATH + "/security")
	accountGroup := e.Group(PATH + "/accounts")
	customerGroup := e.Group(PATH + "/customers")
	cardGroup := e.Group(PATH + "/cards")

	securityService.SetRouters(securityGroup)
	accountService.SetRouters(accountGroup)
	customerService.SetRouters(customerGroup)
	cardService.SetRouters(cardGroup)

}
